import "./App.css";
import UEs from "./components/ues";
import React, { Component } from "react";

class App extends Component {
  state = {
    moyenneGenerale: 0,
    totalEcts: 30,
    ues: [
      {
        nom: "Securité / Reseaux",
        notes: [
          { id: "cc1 Securité", n: 0, coeff: 0.2 },
          { id: "cc2 Securité", n: 0, coeff: 0.3 },
          { id: "cc1 Reseau", n: 0, coeff: 0.2 },
          { id: "cc2 Reseau", n: 0, coeff: 0.3 },
        ],
        moyenne: 0,
        coeff: 6,
      },
      {
        nom: "Optimisation / Modélisation",
        notes: [
          { id: "cc1 Optimisation", n: 0, coeff: 0.2 },
          { id: "cc2 Optimisation", n: 0, coeff: 0.3 },
          { id: "cc1 Modélisation", n: 0, coeff: 0.2 },
          { id: "cc2 Modélisation", n: 0, coeff: 0.3 },
        ],
        moyenne: 0,
        coeff: 6,
      },
      {
        nom: "Compilation",
        notes: [
          { id: "cc1", n: 0, coeff: 0.3 },
          { id: "cc2", n: 0, coeff: 0.3 },
          { id: "cc3", n: 0, coeff: 0.4 },
        ],
        moyenne: 0,
        coeff: 3,
      },
      {
        nom: "Développement mobile",
        notes: [
          { id: "cc1", n: 0, coeff: 0.3 },
          { id: "cc2", n: 0, coeff: 0.3 },
          { id: "cc3", n: 0, coeff: 0.4 },
        ],
        moyenne: 0,
        coeff: 3,
      },
      {
        nom: "UE ouverture",
        notes: [
          { id: "cc1", n: 0, coeff: 0.3 },
          { id: "cc2", n: 0, coeff: 0.3 },
          { id: "cc3", n: 0, coeff: 0.4 },
        ],
        moyenne: 0,
        coeff: 3,
      },
      {
        nom: "Introduction IA",
        notes: [
          { id: "cc1", n: 0, coeff: 0.3 },
          { id: "cc2", n: 0, coeff: 0.3 },
          { id: "cc3", n: 0, coeff: 0.4 },
        ],
        moyenne: 0,
        coeff: 3,
      },
      {
        nom: "PPIL",
        notes: [{ id: "projet", n: 0, coeff: 1 }],
        moyenne: 0,
        coeff: 3,
      },
      {
        nom: "Stage",
        notes: [{ id: "note stage", n: 0, coeff: 1 }],
        moyenne: 0,
        coeff: 3,
      },
    ],
  };

  handleChangeNote = (event, ue, i) => {
    // console.log(event.target.value);
    // console.log(ue);
    console.log(i);
    let ues = [...this.state.ues];
    const index = ues.indexOf(ue);
    let { value, min, max } = event.target;
    console.log(min, max, value);
    if (value !== "")
      Math.max(Number(min), Math.min(Number(max), Number(value)));

    ues[index].notes[i].n = value;
    this.setState({ ues });
    this.calculerMoyenne(ue);
  };

  calculerMoyenne(ue) {
    let m = 0;
    ue.notes.forEach((note) => {
      isNaN(note.n) ? (m += 0) : (m += note.n * note.coeff);
    });

    // console.log(ue.ccs.length);
    m = m.toFixed(2);
    console.log(m);
    let ues = [...this.state.ues];
    const index = ues.indexOf(ue);
    ues[index].moyenne = m;
    this.setState({ ues });
    this.calculerMoyenneGenerale();
  }

  calculerMoyenneGenerale() {
    let m = 0;
    this.state.ues.forEach((ue) => {
      isNaN(ue.moyenne) ? (m += 0) : (m += ue.moyenne * ue.coeff);
    });
    m = m / this.state.totalEcts;
    this.setState({ moyenneGenerale: m });
  }

  render() {
    return (
      <React.Fragment>
        <main className="container">
          <p>Moyenne : {this.state.moyenneGenerale}</p>
          <UEs ues={this.state.ues} onChangeNote={this.handleChangeNote} />
        </main>
      </React.Fragment>
    );
  }
}

export default App;
