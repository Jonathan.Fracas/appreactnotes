import React, { Component } from "react";
import UE from "./ue";

class UEs extends Component {
  render() {
    return (
      <div>
        {this.props.ues.map((ue) => (
          <UE key={ue.nom} ue={ue} onChangeNote={this.props.onChangeNote}></UE>
        ))}
      </div>
    );
  }
}

export default UEs;
