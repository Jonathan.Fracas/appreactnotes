import React, { Component } from "react";

class UE extends Component {
  render() {
    return (
      <div id="ue">
        <p>
          {this.props.ue.nom} coeff : {this.props.ue.coeff}
        </p>
        <p>{this.props.ue.moyenne}</p>
        <div>
          {this.props.ue.notes.map((note) => (
            <ul key={note.id}>
              <p>
                {note.id} Coeff : {note.coeff}
              </p>
              <input
                type="number"
                min="0"
                max="20"
                placeholder="Entrez votre note : "
                value={note.n}
                onChange={(event) =>
                  this.props.onChangeNote(
                    event,
                    this.props.ue,
                    this.props.ue.notes.indexOf(note)
                  )
                }
              ></input>
            </ul>
          ))}
        </div>
      </div>
    );
  }
}

export default UE;
